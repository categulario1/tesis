#!/usr/bin/env python3

if __name__ == '__main__':
    original_pos = 2
    pos = 0

    for i in range(10):
        pos += original_pos/2**i

        print('\t\draw (0,{p}) -- (4,{p});'.format(p=pos))
        print('\t\draw ({p},0) -- ({p},4);'.format(p=pos))
