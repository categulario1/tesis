\begin{chapter}{Algunos ejemplos}

En los textos anteriores se proponen estructuras y teoremas para tratar juegos topológicos y a continuación se dan ejemplos de casos donde se pueden aplicar para obtener algún resultado sobre la ganancia de los jugadores o la presencia de estrategias ganadoras.

\begin{section}{Juegos localmente finitos y números ordinales}

Una de las primeras cosas que durante el trabajo causan duda es el teorema que establece que un juego localmente finito tiene forzosamente un número ordinal asociado, es decir que si $X$ es el espacio de todas las configuraciones posibles del juego y $X_0$ el subconjunto de las posiciones \emph{finales}, existe un ordinal $\alpha$ tal que $X = X(\alpha)$.\\

Para ejemplificar esto de la mejor manera usaré algunos juegos pequeños y muy ilustrativos que luego ampliarán su espacio de configuraciones posibles hasta lo transfinito.

\begin{subsection}{Un juego de monedas}

Este juego se juega en un rectángulo de $1 \times N$ ($N>3$) casillas entre dos jugadores y con tres monedas. Cada jugador mueve una moneda a la vez hacia la izquierda las casillas que quiera. No se pueden saltar otras monedas y no se puede pasar. En cada turno el jugador está obligado a mover una moneda.

\begin{center}
\setlength{\unitlength}{1cm}
\begin{picture}(7, 1)
\linethickness{0.075mm}
\multiput(0,0)(1,0){8} {
	\line(0,1){1}
}
\multiput(0,0)(0,1){2} {
	\line(1,0){7}
}
\multiput(4.5, .5)(1, 0){3} {
	\circle{.8}
}
\end{picture}
\end{center}

El objetivo del juego es hacer que el jugador opuesto no pueda mover más monedas.\\

Con el conjunto de reglas dado es fácil ver que cualquier partida de este juego termina en un número finito de pasos. Es más, se puede calcular el número máximo de jugadas que este juego admite pues suponiendo que cada jugador mueve una moneda solo un cuadro en su turno y sabiendo que cada moneda necesita $N-3$ movimientos para llegar a su posición final, siendo tres monedas sabemos que este juego termina a lo más en $3\cdot(N-3)$ jugadas.\\

La única posición terminal del juego (el único elemento de $X_0 = X(0)$) es la siguiente:

\begin{center}
\setlength{\unitlength}{1cm}
\begin{picture}(7, 1)
\linethickness{0.075mm}
\multiput(0,0)(1,0){8} {
	\line(0,1){1}
}
\multiput(0,0)(0,1){2} {
	\line(1,0){7}
}
\multiput(.5, .5)(1, 0){3} {
	\circle{.8}
}
\end{picture}
\end{center}

pues de lo contrario, si una moneda estuviera una casilla más a la derecha aun se podría hacer un movimiento. De hecho para este juego también es fácil calcular $X(1) = X(0) \union \Gamma^+ X(0)$. Recordemos que $X(1)$ es el conjunto de posiciones terminales mas las posiciones en las que el siguiente turno es terminal forzosamente:

\begin{center}
\setlength{\unitlength}{1cm}
\begin{picture}(8, 3.5)
\linethickness{0.075mm}
\multiput(.5,.5)(1,0){8} {
	\line(0,1){1}
}
\multiput(.5,.5)(0,1){2} {
	\line(1,0){7}
}
\multiput(1, 1)(1, 0){2} {
	\circle{.8}
}
\put(4, 1) {
	\circle{.8}
}

\multiput(.5,2)(1,0){8} {
	\line(0,1){1}
}
\multiput(.5,2)(0,1){2} {
	\line(1,0){7}
}
\multiput(1, 2.5)(1, 0){3} {
	\circle{.8}
}
\end{picture}
\end{center}

Si continuamos este proceso será necesario detenerse en $X(3\cdot(N-3))$, conjunto que ya contiene todas las configuraciones posibles del tablero de este juego.

\end{subsection}

\begin{subsection}{Chomp transfinito}

Aunque ejemplos como el anterior ilustran muy bien la construcción de los $X(\alpha)$, falla en representar el caso transfinito. Para tal propósito considérese el siguiente tablero:

\begin{center}
\setlength{\unitlength}{1cm}
\begin{picture}(4, 4)
\linethickness{0.05mm}
\multiput(0, 0)(4, 0){2} { \line(0, 1){4} }
\multiput(0, 0)(0, 4){2} { \line(1, 0){4} }
\put(2.0, 0) { \line(0, 1){4} }
\put(0, 2.0) { \line(1, 0){4} }
\put(3.0, 0) { \line(0, 1){4} }
\put(0, 3.0) { \line(1, 0){4} }
\put(3.5, 0) { \line(0, 1){4} }
\put(0, 3.5) { \line(1, 0){4} }
\put(3.75, 0) { \line(0, 1){4} }
\put(0, 3.75) { \line(1, 0){4} }
\put(3.875, 0) { \line(0, 1){4} }
\put(0, 3.875) { \line(1, 0){4} }
\put(3.9375, 0) { \line(0, 1){4} }
\put(0, 3.9375) { \line(1, 0){4} }
\put(3.96875, 0) { \line(0, 1){4} }
\put(0, 3.96875) { \line(1, 0){4} }
\put(3.984375, 0) { \line(0, 1){4} }
\put(0, 3.984375) { \line(1, 0){4} }
\put(3.9921875, 0) { \line(0, 1){4} }
\put(0, 3.9921875) { \line(1, 0){4} }
\put(3.99609375, 0) { \line(0, 1){4} }
\put(0, 3.99609375) { \line(1, 0){4} }
\end{picture}
\end{center}

Este tablero está hecho de líneas cada una de las cuales está hacia la derecha o arriba la mitad de la distancia que la línea anterior, hasta el infinito. Por turnos cada jugador escoge una casilla que no haya sido marcada del tablero y se marcan todas las casillas más arriba y más a la derecha de ella:\\

\begin{center}
\tikz[]{
	\draw (0,0) -- (0,4) -- (4,4) -- (4,0) -- cycle;

	\filldraw[fill=gray, draw=gray] (3,2) rectangle (4,4);

	\draw (0,2.0) -- (4,2.0);
	\draw (2.0,0) -- (2.0,4);
	\draw (0,3.0) -- (4,3.0);
	\draw (3.0,0) -- (3.0,4);
	\draw (0,3.5) -- (4,3.5);
	\draw (3.5,0) -- (3.5,4);
	\draw (0,3.75) -- (4,3.75);
	\draw (3.75,0) -- (3.75,4);
	\draw (0,3.875) -- (4,3.875);
	\draw (3.875,0) -- (3.875,4);
	\draw (0,3.9375) -- (4,3.9375);
	\draw (3.9375,0) -- (3.9375,4);
	\draw (0,3.96875) -- (4,3.96875);
	\draw (3.96875,0) -- (3.96875,4);
	\draw (0,3.984375) -- (4,3.984375);
	\draw (3.984375,0) -- (3.984375,4);
	\draw (0,3.9921875) -- (4,3.9921875);
	\draw (3.9921875,0) -- (3.9921875,4);
	\draw (0,3.99609375) -- (4,3.99609375);
	\draw (3.99609375,0) -- (3.99609375,4);

	\filldraw[fill=black, draw=black] (3,2) rectangle (3.5,3);
}
\end{center}

El objetivo es hacer que el jugador contrario escoja el cuadro de la esquina inferior izquierda (el más grande).\\

Para ver que este juego que es localmente finito podemos asignar un índice a cada casilla según su fila y columna, contando las filas y las columnas desde abajo y desde la izquierda respectivamente.
Supóngase que $A = \{(m_i, n_i)\}_{i \in \naturals}$ es el conjunto de casillas seleccionadas durante el juego. Si probamos que
$$
\begin{array}{rcl}
M & = & \displaystyle\max_{(m_i, n_i) \in A} m_i < \infty,\\
N & = & \displaystyle\max_{(m_i, n_i) \in A} n_i < \infty
\end{array}
$$
entonces sabremos que el juego duró a lo más $M \times N$ jugadas, pues todas las casillas con fila mayor que $M$ y con columna mayor que $N$ ya habrán sido descartadas.\\

\begin{center}
\setlength{\unitlength}{1cm}
\begin{picture}(4.5, 4.5)(-.5, -.5)
\linethickness{0.05mm}
\multiput(0, 0)(4, 0){2} { \line(0, 1){4} }
\multiput(0, 0)(0, 4){2} { \line(1, 0){4} }
\put(2.0, 0) { \line(0, 1){4} }
\put(0, 2.0) { \line(1, 0){4} }
\put(3.0, 0) { \line(0, 1){4} }
\put(0, 3.0) { \line(1, 0){4} }
\put(3.5, 0) { \line(0, 1){4} }
\put(0, 3.5) { \line(1, 0){4} }
\put(3.75, 0) { \line(0, 1){4} }
\put(0, 3.75) { \line(1, 0){4} }
\put(3.875, 0) { \line(0, 1){4} }
\put(0, 3.875) { \line(1, 0){4} }
\put(3.9375, 0) { \line(0, 1){4} }
\put(0, 3.9375) { \line(1, 0){4} }
\put(3.96875, 0) { \line(0, 1){4} }
\put(0, 3.96875) { \line(1, 0){4} }
\put(3.984375, 0) { \line(0, 1){4} }
\put(0, 3.984375) { \line(1, 0){4} }
\put(3.9921875, 0) { \line(0, 1){4} }
\put(0, 3.9921875) { \line(1, 0){4} }
\put(3.99609375, 0) { \line(0, 1){4} }
\put(0, 3.99609375) { \line(1, 0){4} }
\put(1, -.35) {1}
\put(2.5, -.35) {2}
\put(3.25, -.35) {3}
\put(3.625, -.35) {4}
\put(3.8125, -.35) {5}
\put(3.90625, -.35) {6}
\put(3.953125, -.35) {7}
\put(3.9765625, -.35) {8}
\put(3.98828125, -.35) {9}
\put(-.2, 0.9) {1}
\put(-.2, 2.4) {2}
\put(-.2, 3.15) {3}
\put(-.2, 3.525) {4}
\put(-.2, 3.7125) {5}
\put(-.2, 3.80625) {6}
\put(-.2, 3.853125) {7}
\put(-.2, 3.8765625) {8}
\put(-.2, 3.88828125) {9}
\put(-.2, 3.894140625) {10}
\end{picture}
\end{center}

Si la primera jugada de la partida es en $(m_0, n_0)$ entonces la única forma de elegir una casilla con un índice de fila más grande es en la región $n < n_0$ que contiene $n_0-1$ columnas, cada que se seleccione una columna no se podrán seleccionar números de fila más grandes que el número de fila seleccionado en esa columna, así que se pueden seleccionar a lo más $n_0-1$ veces un número más grande que $m_0$ y entonces el máximo número de fila existe y es finito. El argumento es simétrico para las columnas, así que $M$ y $N$ son finitos y las partidas del juego son finitas, luego entonces este juego tiene un número ordinal asociado.\\

Una característica importante de este juego es que, para cualquier $n \in \naturals$, se puede jugar una partida con más de $n$ posiciones. Tómese $n \in \naturals$ arbitrario, si el primer jugador juega $(2, 1)$, el segundo juega $(1, n)$ y a partir de ahí seleccionan las casillas $(1, n-1), (1, n-2), \dots, (1, 1)$ entonces se habrá jugado una partida de $n+1$ posiciones.\\

\begin{center}
\tikz[]{
	\draw (0,0) -- (0,4) -- (4,4) -- (4,0) -- cycle;

	\filldraw[fill=gray, draw=gray] (0,2) rectangle (4,4);
	\filldraw[fill=gray, draw=gray] (3.5,0) rectangle (4,4);

	\draw (0,2.0) -- (4,2.0);
	\draw (2.0,0) -- (2.0,4);
	\draw (0,3.0) -- (4,3.0);
	\draw (3.0,0) -- (3.0,4);
	\draw (0,3.5) -- (4,3.5);
	\draw (3.5,0) -- (3.5,4);
	\draw (0,3.75) -- (4,3.75);
	\draw (3.75,0) -- (3.75,4);
	\draw (0,3.875) -- (4,3.875);
	\draw (3.875,0) -- (3.875,4);
	\draw (0,3.9375) -- (4,3.9375);
	\draw (3.9375,0) -- (3.9375,4);
	\draw (0,3.96875) -- (4,3.96875);
	\draw (3.96875,0) -- (3.96875,4);
	\draw (0,3.984375) -- (4,3.984375);
	\draw (3.984375,0) -- (3.984375,4);
	\draw (0,3.9921875) -- (4,3.9921875);
	\draw (3.9921875,0) -- (3.9921875,4);
	\draw (0,3.99609375) -- (4,3.99609375);
	\draw (3.99609375,0) -- (3.99609375,4);

	\draw (1,2.5) node {1};
	\draw (3.6,1) node {2};
	\draw (1,1) node {$n+1$};
	\draw (2.7,1) node {$\dots$};
}
\end{center}

Entonces ¿cuál es el número ordinal de este juego? Sabemos que no puede ser un ordinal finito pues se acaba de decir que es posible construir una partida más larga siempre, así que si $\alpha$ es tal que $X(\alpha)$ es el espacio de todas las configuraciones posibles de este juego entonces a lo menos $\alpha \geq \omega$ \footnote{En la jerga de la inducción transfinita $\omega$ es usado para describir el primer ordinal de cardinalidad infinita, asociado a $\naturals$}. Sin embargo, supongamos que $\alpha = \omega$, como $\omega$ es un ordinal límite entonces la definición
$$
X(\omega) = \bigunion_{\beta < \omega} X(\beta)
$$
nos dice que el espacio de configuraciones de este juego es tal que la primera jugada $x \in X(\omega)$ forzosamente cumple $x \in X(\beta)$ para algún $\beta$ finito. Lo cual querría decir que después de la primera jugada solo podría haber a lo más $\beta$ jugadas más, lo cual no es cierto y entonces $\alpha > \omega$. \footnote{Lo más probable es que en este caso $\alpha=\omega^2$ pero aun no puedo probarlo.}\\

\end{subsection}

\end{section}

\begin{section}{Juegos topológicos y ganancias}

En esta sección se estudian juegos con una estructura topológica heredada de la forma del tablero y en los cuales se pueden aplicar los teoremas sobre estrategias óptimas.

\begin{subsection}{Persecución en el mar}

Considérese a $(M, d)$ un espacio métrico, \emph{el mar}, y tres jugadores denominados $(1)$, $(2)$ y $(3)$. En este juego de persecución $(1)$ y $(2)$ persiguen a $(3)$. La posición del jugador $(i)$ estará dada por $m_i$.\\

El objetivo de $(1)$ es lograr $m_1 = m_3$, el objetivo de $(2)$ es minimzar $d(m_2, m_3)$ y el objetivo de $(3)$ es lograr $m_3 \neq m_1$ y $m_3 \neq m_1$.\\

Un estado del juego estará dado por
$$
x = (m_1, m_2, m_3, \theta) \in M \times M \times M \times \Theta = X
$$
donde $\Theta = \{1, 2, 3\}$ y las particiones de $X$ son
$$
X_i = M \times M \times M \times \{i\}
$$

Se denotará como $X_0 = \{(m_1, m_2, m_3, \theta) \in X : m_1=m_3 \text{ ó } m_2 = m_3 \}$ el conjunto de posiciones que terminan el juego. Definimos $\Gamma: X \to 2^X$ como
$$
\Gamma(x) = \left\lbrace
	\begin{array}{lr}
		\phi & x \in X_0 \\
		\overline{B}_{r_1}(m_1) \times \{m_2\} \times \{m_3\} \times \{2\} & \theta = 1 \\
		\{m_1\} \times \overline{B}_{r_2}(m_2) \times \{m_3\} \times \{3\} & \theta = 2 \\
		\{m_1\} \times \{m_2\} \times \overline{B}_{r_3}(m_3) \times \{1\} & \theta = 3
	\end{array}
\right.
$$
donde cada $r_i$ es la distancia máxima que cada jugador puede avanzar en un turno y las funciones de ganancia de los jugadores son
$$
\begin{array}{lr}
	f_1(x) $ = $ \left\lbrace
		\begin{array}{rl}
			1 & d(m_1, m_3) < r_1 \\
			0 & d(m_1, m_3) \geq r_1
		\end{array}
	\right. \\

	f_2(x) $ = $ -d(m_2, m_3) \\

	f_3(x) $ = $ \left\lbrace
		\begin{array}{rl}
			1 & d(m_1, m_3) \geq r_1 $ y $ d(m_2, m_3) \geq r_2 \\
			0 & d(m_1, m_3) < r_1 $ ó $ d(m_2, m_3) < r_2
		\end{array}
	\right.
\end{array}
$$
En este caso $N^+ = \{(1), (2)\}$ y $N^- = \{(3)\}$.\\

Es fácil ver que con estas características se tiene un juego topológico que cumple las cuatro propiedades definidas en \ref{def:game} ya que
\begin{enumerate}
	\item $N^+$ y $N^-$ dados son una partición válida del conjunto de jugadores,
	\item $X_i = M \times M \times M \times \{i\}$ es un espacio topológico con la topología suma y $M$ al ser métrico es topológico con la topología inducida por su métrica,
	\item el mapeo $\Gamma$ dado cumple por construcción que si $x \in X_i$ entonces $\Gamma x \intersection X_i = \phi$,
	\item cada función $f_i$ definida es real, acotada y definida en $X$.
\end{enumerate}
De esta manera $\game = (X, \Gamma, N, \{f_i\}_{i \in N})$ es un juego topológico.\\

Si lo que quisieramos ahora es usar el Teorema \ref{thm:opt_strategies} para decir que existe una estrategia óptima para $(3)$ faltaría probar que en este juego $f_3$ es superiormente semicontinua. También se necesita $\Gamma x$ compacto si $x \in X_3 \minus X_0$ pero por la forma en que se define $\Gamma$ en bolas cerradas que resultan acotadas y suponiendo que el espacio métrico $M$ es completo y totalmente acotado entonces $\Gamma x$ es compacto.\\

Para resolver el primer asunto recordemos que $f_3$ es superiormente semicontinua en $x_0$ si para cada $\epsilon > 0$ existe una vecindad abierta $U$ de $x_0$ tal que
$$
f_3(x) \leq f_3(x_0) + \epsilon
$$
para toda $x \in U$. Sea entonces $x_0 \in X$, pueden pasar dos cosas. En el primer caso $f_3(x_0) = 1$ y entonces como los únicos dos valores de $f_3$ son 0 y 1, una vecindad abierta que cumple la condición es $U = X$. En el segundo caso $f_3(x_0) = 0$, lo cual significa que $d(m_1, m_3) < r_1 $ ó $ d(m_2, m_3) < r_2$. Sin pérdida de generalidad supongamos lo primero, $d(m_1, m_3) < r_1$, entonces $m_1 \in B_{r_1}(m_3)$ y $m_3 \in B_{r_1}(m_1)$, como ambos conjuntos son abiertos $B_{r_1}(m_1) \intersection B_{r_1}(m_3)$ es abierta y existen sendas bolas abiertas con centro en $m_1$ y $m_2$ y radios $\epsilon_1$ y $\epsilon_2$ completamente contenidas en la intersección.\\

A estas bolas además les podemos pedir que tengan radio estrictamente menor a $\frac{r_1 - d(m_1, m_3)}{2}$. Así, si tomamos dos puntos $x$ e $y$ en $B_{\epsilon_1}(m_1)$ y $B_{\epsilon_2}(m_3)$ respectivamente se va a cumplir que
$$
\begin{array}{rcl}
	d(x, y) & \leq & d(x, m_1) + d(m_1, m_3) + d(m_3, y) \\
	        &   <  & \epsilon_1 + d(m_1, m_3) + \epsilon_2 \\
	        &   <  & \frac{r_1 - d(m_1, m_3)}{2} + d(m_1, m_3) + \frac{r_1 - d(m_1, m_3)}{2} \\
	        &   =  & r_1,
\end{array}
$$
lo cual indica que si hacemos $U = B_{\epsilon_1}(m_1) \times M \times B_{\epsilon_2}(m_2) \times \Theta$ entonces para todas las configuraciones del juego $x \in U$ se tiene $0 = f_3(x) \leq f_3(x_0) + \epsilon = \epsilon$. Lo cual concluye que $f_3$ es superior semicontinua.\\

Luego podemos aplicar el Teorema \ref{thm:opt_strategies} y decir que existe una estrategia óptima $\sigma_3$ para $(3)$.

\end{subsection}

\begin{subsection}{Persecución en una malla}

En este juego considérese una malla formada por el producto $M = \naturals \times \naturals$ en la que se tienen dos jugadores: $(1) \in N^+$ que persigue y $(2) \in N^-$ que es perseguido, sus posiciones están dadas por $m_1 = (i_1, j_1)$, $m_2 = (i_2, j_2)$. Aquí el conjunto de posibles configuraciones del juego es el espacio $X = M \times M \times \Theta$, donde $\Theta = \{(1), (2)\}$, y $M$ tiene la métrica definida por
$$
d(m_1, m_2) = \max\{|i_1 - i_2|, |j_1-j_2|\}.
$$

Las funciones de ganancia de los jugadores están dadas por
$$
\begin{array}{rcl}
	f_1(x) & = & \left\lbrace\begin{array}{rl}
		1 & j_1 \geq j_2 $ ó $ i_1 \geq i_2 \\
		0 & j_1 < j_2 $ ó $ i_1 < i_2
	\end{array}\right.\\
	f_2(x) & = & 1 - f_1(x)
\end{array}
$$
y $\Gamma$ está definida como
$$
\Gamma x = \left\lbrace\begin{array}{ll}
	\phi & j_1 \geq j_2 $ ó $ i_1 \geq i_2 \\
	\{(i, j) | r_m \leq i-i_1 \leq r_M,r_m \leq j-j_1 \leq r_M \} \times \{m_2\} \times \{2\} & \theta = 1\\
	\{m_1\} \times \overline{B}_{r_2}(m_2) \times \{1\} & \theta = 2\\
\end{array}\right.
$$
donde $r_m$ y $r_M$ son enteros positivos ($r_m < r_M$) que limitan el movimiento de $(1)$ y $r_2$ limita el movimiento de $(2)$.

\begin{center}
\tikz[scale=0.5]{
	\foreach \x in {0,...,19} \draw (0,\x) -- (20,\x);
	\foreach \y in {0,...,19} \draw (\y,0) -- (\y,20);

	\filldraw (3,4) circle(2pt) node[align=right,below]{$x_1$};
	\filldraw[fill=gray, draw=gray] (3,7) rectangle (6,9);
	\filldraw[fill=gray, draw=gray] (6,4) rectangle (8,9);

	\filldraw[fill=gray, draw=gray] (12,10) rectangle (16,14);
	\filldraw (14,12) circle(2pt) node[align=right,below]{$x_2$};
}
\end{center}

Al igual que en la persecución en el mar este juego cumple las condiciones de un juego topológico, pero además si $r_m > r_2$ se tiene que el juego es localmente finito pues sin importar como jueguen ambos jugadores cada par de turnos, su distancia en alguno de los ejes va a disminuir por lo menos $r_m - r_2$. Si $d_i$ es la diferencia en el eje $i$ entre los jugadores y $d_j$ la diferencia en el eje $j$ entonces existen $I$, $J$ enteros tales que $I\cdot(r_m - r_2) > d_i$ y $J\cdot(r_m - r_2) > d_j$, y el juego terminará en a lo más $I+J$ pares de turnos.\\

Restaría ver que este juego es topológico superior para $(1) \in N^+$, que $\Gamma x$ es compacto y que $X_0$ es un conjunto abierto. Nótese que como el espacio es discreto\footnote{Con la métrica elegida las bolas de radio 1 tienen solo un elemento y $\Gamma x$ siempre es finito.} todas las condiciones se cumplen trivialmente así que podemos aplicar el Teorema \ref{thm:opt_strategies} para los jugadores $(1)$ y $(2)$ y saber que existe un punto de equilibrio $\sigma = (\sigma_1, \sigma_2)$.\\

\end{subsection}

\end{section}

\begin{section}{Teorema de Zermelo}

\begin{subsubsection}{Chomp}

Ejemplificar el teorema de Zermelo es muy sencillo si se considera el Corolario \ref{crl:zermelo}. Tómese por ejemplo el juego de chomp transfinito, este es localmente finito y no puede terminar en empate pues al final alguien habrá tenido que seleccionar el cuadro de la esquina inferior izquierda. El teorema de Zermelo dice entonces que existe una estrategia ganadora para uno de los jugadores.\\

De hecho la estrategia es muy sencilla si se considera la simetría del tablero: El primer jugador tiene que jugar la posición 2,2 al principio y luego solamente replicar las jugadas del oponente del lado opuesto de la diagonal $(m, m)$.

\begin{center}
\tikz[]{
	\draw (0,0) -- (0,4) -- (4,4) -- (4,0) -- cycle;

	\filldraw[fill=gray, draw=gray] (2,2) rectangle (4,4);
	\filldraw[fill=black, draw=black] (2,2) rectangle (3,3);

	\draw (0,2.0) -- (4,2.0);
	\draw (2.0,0) -- (2.0,4);
	\draw (0,3.0) -- (4,3.0);
	\draw (3.0,0) -- (3.0,4);
	\draw (0,3.5) -- (4,3.5);
	\draw (3.5,0) -- (3.5,4);
	\draw (0,3.75) -- (4,3.75);
	\draw (3.75,0) -- (3.75,4);
	\draw (0,3.875) -- (4,3.875);
	\draw (3.875,0) -- (3.875,4);
	\draw (0,3.9375) -- (4,3.9375);
	\draw (3.9375,0) -- (3.9375,4);
	\draw (0,3.96875) -- (4,3.96875);
	\draw (3.96875,0) -- (3.96875,4);
	\draw (0,3.984375) -- (4,3.984375);
	\draw (3.984375,0) -- (3.984375,4);
	\draw (0,3.9921875) -- (4,3.9921875);
	\draw (3.9921875,0) -- (3.9921875,4);
	\draw (0,3.99609375) -- (4,3.99609375);
	\draw (3.99609375,0) -- (3.99609375,4);
}
\end{center}

Aunque el segundo jugador puede elegir en cuántas jugadas perder, no puede cambiar el resultado del juego.

\end{subsubsection}

\begin{subsubsection}{Supergato}

El juego de supergato no es más que una versión más interesante del gato tradicional, en la que se construye un tablero grande con tableros pequeños en cada casilla como en la imagen que aparece a continuación.

\begin{center}
\setlength{\unitlength}{.5cm}
\begin{picture}(11,11)
\linethickness{1mm}
\multiput(3.5, 0)(4, 0){2} { \line(0, 1){11} }
\multiput(0, 3.5)(0, 4){2} { \line(1, 0){11} }

\linethickness{.5mm}
\multiput(1, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 1)(0, 1){2} { \line(1, 0){3} }

\multiput(5, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 5)(0, 1){2} { \line(1, 0){3} }

\multiput(9, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 9)(0, 1){2} { \line(1, 0){3} }

\multiput(4, 1)(0, 1){2} { \line(1, 0){3} }
\multiput(1, 4)(1, 0){2} { \line(0, 1){3} }

\multiput(8, 1)(0, 1){2} { \line(1, 0){3} }
\multiput(1, 8)(1, 0){2} { \line(0, 1){3} }

\multiput(5, 4)(1, 0){2} { \line(0, 1){3} }
\multiput(4, 5)(0, 1){2} { \line(1, 0){3} }

\multiput(9, 4)(1, 0){2} { \line(0, 1){3} }
\multiput(4, 9)(0, 1){2} { \line(1, 0){3} }

\multiput(8, 5)(0, 1){2} { \line(1, 0){3} }
\multiput(5, 8)(1, 0){2} { \line(0, 1){3} }

\multiput(9, 8)(1, 0){2} { \line(0, 1){3} }
\multiput(8, 9)(0, 1){2} { \line(1, 0){3} }
\end{picture}
\end{center}

El juego va como sigue. El primer jugador selecciona cualquier casilla dentro de cualquier gato pequeño y la marca con su símbolo ya sea una $\times$ o un $\circ$. A partir de ahí cada siguiente jugada se debe jugar en el tablero pequeño que está en la misma posición respecto al tablero grande que la casilla que acaba de jugar el jugador previo respecto al gato pequeño en que la jugó. A continuación un ejemplo

\begin{center}
\setlength{\unitlength}{.5cm}
\begin{picture}(11,11)
\linethickness{1mm}
\multiput(3.5, 0)(4, 0){2} { \line(0, 1){11} }
\multiput(0, 3.5)(0, 4){2} { \line(1, 0){11} }

\linethickness{.5mm}
\multiput(1, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 1)(0, 1){2} { \line(1, 0){3} }

\multiput(5, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 5)(0, 1){2} { \line(1, 0){3} }

\multiput(9, 0)(1, 0){2} { \line(0, 1){3} }
\multiput(0, 9)(0, 1){2} { \line(1, 0){3} }

\multiput(4, 1)(0, 1){2} { \line(1, 0){3} }
\multiput(1, 4)(1, 0){2} { \line(0, 1){3} }

\multiput(8, 1)(0, 1){2} { \line(1, 0){3} }
\multiput(1, 8)(1, 0){2} { \line(0, 1){3} }

\multiput(5, 4)(1, 0){2} { \line(0, 1){3} }
\multiput(4, 5)(0, 1){2} { \line(1, 0){3} }

\multiput(9, 4)(1, 0){2} { \line(0, 1){3} }
\multiput(4, 9)(0, 1){2} { \line(1, 0){3} }

\multiput(8, 5)(0, 1){2} { \line(1, 0){3} }
\multiput(5, 8)(1, 0){2} { \line(0, 1){3} }

\multiput(9, 8)(1, 0){2} { \line(0, 1){3} }
\multiput(8, 9)(0, 1){2} { \line(1, 0){3} }

\put(2.45, 1.25) {$\times$}
\put(3, 1.5) { \vector(2,1){5} }
\end{picture}
\end{center}

\noindent
donde se muestra con una $\times$ la última jugada y se señala con una flecha qué tablero pequeño debe jugar el oponente.\\

Si un tablero pequeño se gana o empata con las reglas del gato tradicional entonces cualquier jugada que caiga ahí le da al jugador la habilidad de tirar en cualquier casilla disponible de cualquier gato pequeño (que no esté cerrado). El objetivo es ganar el gato grande con las reglas del gato usual, pero para ganar una casilla del gato grande es necesario haber ganado el correspondiente tablero pequeño. Puede encontrarse una descripción más amplia en \cite{web:uttt}.\\

Aunque en este juego es posible tener empates, con un pequeño cambio en los objetivos de los jugadores se puede aplicar el Teorema de Zermelo. Considérese que el objetivo del jugador $(1)$ es ganar, es decir
$$
f_1(x) = \left\lbrace\begin{array}{rl}
	1 & $si $x$ es una posición ganadora para $(1) \\
	0 & $en cualquier otro caso$
\end{array}\right.
$$
pero que el jugador $(2)$ gana si el tablero se empata:
$$
f_2(x) = \left\lbrace\begin{array}{rl}
	1 & $si $x$ es una posición de empate o de victoria para $(2) \\
	0 & $en cualquier otro caso$
\end{array}\right.
$$
Entonces este es un juego de información perfecta, localmente finito que \emph{no puede terminar en empate} y por ende tiene una estrategia ganadora para alguno de los dos jugadores. Si la estrategia es para $(1)$ entonces tiene una estrategia ganadora realmente y si la estrategia es para $(2)$ entonces quiere decir que siempre es posible asegurar un empate en este juego.

\end{subsubsection}

\end{section}

\end{chapter}
