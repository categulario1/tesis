\begin{chapter}{Juegos, ganancias y estrategias}

\begin{section}{Ganancias}
Algo muy importante en un juego es saber si es posible ganar, ¿que no?, pero también es importante poder controlar la ganancia que se obtiene del juego. En este capítulo se van a estudiar las condiciones necesarias sobre los jugadores y el juego para poder garantizar una ganancia determinada.\\

El primer lema de este capítulo es una herramienta que justifica alguno de los pasos delicados en las demostraciones de este capítulo.\\

\begin{theorem}
\label{thm:1}
Si un juego es inferiormente topológico para el jugador $(1) \in N^+$, el conjunto de posiciones para las cuales $(1)$ puede garantizar una ganancia estricta $\gamma$ es abierto en $X$.
\end{theorem}

\begin{proof}
Sea $G_\gamma$ el conjunto de posiciones iniciales desde las cuales $(1)$ puede garantizar una ganancia estricta $\gamma$. Construiremos por inducción transfinita un conjunto abierto $G(\alpha)$ tal que $G(\alpha) \subset G_\gamma$ para cada ordinal $\alpha$ y $G(\alpha) \subset G(\beta)$ si $\alpha < \beta$.\\

Sea
$$
G(0) = \{x: f_1(x) > \gamma\}
$$
Como $f_1$ es inferiormente semicontinua $G(0)$ es abierto y $G(0) \subset G_\gamma$. Ahora supóngase que hemos definido conjuntos abiertos $G(\beta) \subset G_\gamma$ para cada ordinal $\beta < \alpha$.\\

Si $\alpha$ es un ordinal límite sea
$$
G(\alpha) = \bigunion_{\beta < \alpha} G(\beta),
$$
claramente $G(\alpha)$ es abierto y $G(\alpha) \subset G_\gamma$. Por otro lado si $\alpha = \alpha'+1$ para algún ordinal $\alpha'$ sea
$$
G(\alpha) = G(\alpha') \union (X_1 \intersection \Gamma^- G(\alpha')) \union \bigunion_{j=2}^n (X_j \intersection \Gamma^+ G(\alpha'))
$$
De esta expresión $G(\alpha')$ es abierto por hipótesis y los otros dos componentes $(X_1 \intersection \Gamma^- G(\alpha'))$ y $\bigunion_{j=2}^n (X_j \intersection \Gamma^+ G(\alpha'))$ son abiertos ya que $X$ es la suma topológica de los $X_k$ y $\Gamma$ es continua. Así $G(\alpha)$ es abierto y $G(\alpha) \subset G_\gamma$ ya que $G(\alpha') \subset G_\gamma$ y
$$
(X_1 \intersection \Gamma^- G(\alpha')) \union \bigunion_{j=2}^n (X_j \intersection \Gamma^+ G(\alpha')) \subset G_\gamma.
$$

La secuencia transfinita $\{G(\alpha)\}$ es creciente y cada conjunto está contenido en $G_\gamma$, así por el Lema \ref{lm:star} (\emph{Estabilidad para sucesiones transfinitas acotadas crecientes}) debe ser eventualmente constante, es decir, $G(\alpha_0) = G(\alpha + 1) = \dots$ para algún $\alpha_0$.\\

Sea $G' = X \minus G(\alpha_0)$. Si $x \in G' \intersection X_1$ entonces $\Gamma x \subset G'$ ya que todas las posiciones que llevan a un punto de $G(\alpha_0)$ desde $X_1$ están en $G(\alpha_0)$, mientras que si $x \in G' \intersection X_j$, $j \neq 1$, $\Gamma x \intersection G' \neq \phi$ ya que los $x \in X_j$ que no llevan a $G'$ están en $G(\alpha_0)$.\\

Esto quiere decir que si una partida comienza en $G'$, sin importar qué haga $(1)$, el resto de los jugadores puede asegurar que nunca se cae en una casilla de $G(\alpha_0)$. Así, si $x \in G',\, x \notin G_\gamma$, entonces $G_\gamma \subset G(\alpha_0)$ y como $G(\alpha_0) \subset G_\gamma$ tenemos $G_\gamma = G(\alpha_0)$. Esto concluye la demostración de que $G_\gamma$ es abierto en $X$.
\end{proof}

\begin{theorem}
\label{thm:2}
Supóngase que un juego es localmente finito y topológico superior para $(1) \in N^+$ y que $X_0$ \footnote{Definido al mismo tiempo que un juego topológico en la definición \ref{def:game}} es un conjunto abierto. Entonces el conjunto de posiciones desde las cuales $(1)$ puede garantizar una ganancia $\gamma$ es cerrado.
\end{theorem}

\begin{proof}
Primero se han de construir conjuntos $X(\alpha)$ para cada ordinal con la única propiedad de ser abiertos y cumplir las condiciones

\begin{itemize}
	\item $\beta < \alpha \implies X(\beta) \subset X(\alpha)$,
	\item si $x \in X(\alpha)$ y $\Gamma x \neq \phi$ entonces $\Gamma x \subset X(\beta)$ para algún $\beta < \alpha$.
\end{itemize}

Sea $X(0) = X_0 = \{ x \in X | \Gamma x = \phi \}$, el cual es abierto por definición y cumple las propiedades por nulidad. Ahora supóngase que se han construido abiertos $X(\beta)$ cumpliendo los puntos anteriores para cada ordinal $\beta < \alpha$. Si $\alpha$ es un ordinal límite sea
$$
X(\alpha) = \bigunion_{\beta < \alpha} X(\beta),
$$
que es abierto por ser unión arbitraria de abiertos y además cumple la primera propiedad.\\
Respecto a la segunda, tómese $x \in X(\alpha)$. Por construcción de $X(\alpha)$, $x$ está en $X(\beta)$ para algún $\beta < \alpha$ y por hipótesis $X(\beta)$ cumple ya la condición. Así $\Gamma x \subset X(\beta')$ para algún $\beta' < \beta < \alpha$.\\

Ahora si $\alpha$ es un sucesor, digamos, de $\alpha'$. Sea
$$
X(\alpha) = X(\alpha') \union \Gamma^+ X(\alpha'),
$$
que es abierto, ya que $\Gamma$ es continua y entonces $\Gamma^+ X(\alpha')$ es abierto. Se ve a leguas que $X(\alpha') \subset X(\alpha)$ y por transitividad la primera propiedad se cumple para cualquier $\beta < \alpha'$.

Si tomamos $x \in X(\alpha')$, con $\Gamma x \neq \phi$, la segunda propiedad se cumple para $X(\alpha)$ por hipótesis de inducción. En cambio si tomamos $x \in \Gamma^+ X(\alpha') \minus X(\alpha')$ entonces por definición de $\Gamma^+$ (Definición \ref{def:inv}), $\Gamma x \subset X(\alpha')$.\\

Así por inducción transfinita existen $X(\alpha)$ para cada ordinal que cumplen las condiciones dadas.\\

\begin{center}
***
\end{center}

Sea $F_\gamma$ el conjunto de posiciones desde las cuales $(1)$ puede garantizar una ganancia $\gamma$.\\

Se definirán conjuntos $F(\alpha)$ tales que
\begin{enumerate}
	\item $F(\alpha) \subset F_\gamma$,
	\item si $\beta < \alpha$, $F(\beta) \subset F(\alpha)$,
	\item si $\beta < \alpha$, $F(\alpha) \intersection X(\beta) = F(\beta) \intersection X(\beta)$,
	\item $F(\alpha) \intersection X(\alpha)$ es cerrado en $X(\alpha)$,
\end{enumerate}
para cada ordinal $\alpha$.\\

Sea $F(0) = \{ x \in X | f_1(x) \geq \gamma \}$, que por nulidad cumple las primeras tres propiedades y además $F(0)$ es cerrado pues $f_1$ es superior semicontinua \footnote{Es un argumento sencillo en topología que el complemento de este conjunto sea abierto dada la definición de semicontinuidad superior. Luego entonces este conjunto es cerrado}.\\

Supóngase que se han construido conjuntos $F(\beta)$ que satisfacen las cuatro condiciones para cada ordinal $\beta < \alpha$. Si $\alpha$ es un ordinal límite sea
$$
F(\alpha) = \bigunion_{\beta < \alpha} F(\beta)
$$
La primera y segunda propiedades se cumplen ya que cada $F(\beta)$ está contenido en $F(\alpha) \subset F_\gamma$. Para la tercera propiedad considérese $\beta' < \alpha$, entonces
\begin{align*}
F(\alpha) \intersection X(\beta') &= \left(\bigunion_{\beta < \alpha} F(\beta)\right) \intersection X(\beta') \\
	&= \bigunion_{\beta < \alpha} \left( F(\beta) \intersection X(\beta') \right).
\end{align*}
Si $\alpha > \beta > \beta'$ entonces por hipótesis de inducción $F(\beta) \intersection X(\beta') = F(\beta') \intersection X(\beta')$. Por otro lado si $\beta \leq \beta'$ la segunda propiedad nos da $F(\beta) \subset F(\beta')$ lo cual implica que $F(\beta) \intersection X(\beta') \subset F(\beta') \intersection X(\beta')$.

Aplicando esto a cada término de la unión obtenemos
$$
F(\alpha) \intersection X(\beta') \subset F(\beta') \intersection X(\beta')
$$
Pero también como $\beta' < \alpha \implies F(\beta') \subset F(\alpha)$, entonces tenemos la contención opuesta y se concluye que
$$
F(\alpha) \intersection X(\beta') = F(\beta') \intersection X(\beta').
$$\\

Para probar que $F(\alpha)$ es cerrado tómese $x \in X(\alpha) \minus F(\alpha)$. Como $x \in X(\alpha)$, entonces $x \in X(\beta)$ para algún $\beta < \alpha$ y $x \notin F(\beta)$ pues $F(\beta) \subset F(\alpha)$. Por hipótesis $F(\beta) \intersection X(\beta)$ es cerrado en $X(\beta)$ y entonces existe una vecindad abierta $U$ de $x$ en $X(\beta)$ tal que $U \intersection F(\beta) = \phi$.

Así $x \in U \subset X(\beta) \subset X(\alpha)$ y como $F(\alpha) \intersection X(\beta) = F(\beta) \intersection X(\beta)$ entonces $U$ es una vecindad abierta de $x$ en $X(\alpha)$ tal que $U \intersection F(\alpha) = \phi$.

Con esto hemos probado que el complemento de $F(\alpha)$ en $X(\alpha)$ es abierto en $X(\alpha)$, de donde se concluye que $F(\alpha)$ es cerrado en $X(\alpha)$.\\

Ahora suponga que $\alpha$ tiene predecesor $\alpha'$, sea
$$
F(\alpha) = F(\alpha') \union \left( X_1 \intersection \Gamma^- F(\alpha') \right) \union \bigunion_{j = 2}^n (X_j \intersection \Gamma^+ F(\alpha)).
$$

La primera condición se cumple ya que por hipótesis $F(\alpha') \subset F_\gamma$ y sabemos que si $A \subset B$, entonces $\Gamma^- A \subset \Gamma^- B$ y también $\Gamma^+ A \subset \Gamma^+ B$. Así que podemos afirmar
$$
(X_1 \intersection \Gamma^- F(\alpha')) \union \bigunion_{j=2}^n (X_j \intersection \Gamma^+ F(\alpha')) \subset (X_1 \intersection \Gamma^- F_\gamma) \union \bigunion_{j=2}^n (X_j \intersection \Gamma^+ F_\gamma)
$$
Ahora $(X_1 \intersection \Gamma^- F_\gamma) \subset F_\gamma$ ya que $X_1 \intersection \Gamma^- F_\gamma$ son las casillas $x$ en las que $(1)$ tiene la iniciativa y en las que $\Gamma x \intersection F_\gamma \neq \phi$ (quiere decir que $(1)$ puede elegir una casilla en $F_\gamma$ desde $x$. Por otro lado $\bigunion_{j=2}^n (X_j \intersection \Gamma^+ F_\gamma) \subset F_\gamma$ ya que se trata de las casillas en las que la iniciativa corresponde a alguno de los otros jugadores y donde $\Gamma x \subset F_\gamma$ (cualquier elección cae en $F_\gamma$). Concluyendo así
$$
F(\alpha) \subset F_\gamma.
$$\\

La segunda propiedad se cumple ya que $F(\alpha') \subset F(\alpha)$ por construcción.\\

Para ver que se cumple la tercera propiedad tómese $\beta' < \alpha$. Obsérvese que como $\beta' < \alpha$ entonces $F(\beta') \subset F(\alpha)$ y entonces $F(\beta') \intersection X(\beta') \subset F(\alpha) \intersection X(\beta')$. Resta probar la contención contraria. Desarrollando el término derecho se obtiene
$$
F(\alpha) \intersection X(\beta') = \left[ F(\alpha') \intersection X(\beta') \right] \union \left[X_1 \intersection \Gamma^- F(\alpha') \intersection X(\beta') \right] \union \left[ \bigunion_{j=2}^n (X_j \intersection \Gamma^+ F(\alpha') \intersection X(\beta')) \right]
$$
Analizamos esta expresión por partes. Sabemos que $F(\alpha') \intersection X(\beta') = F(\beta') \intersection X(\beta')$ por hipótesis de inducción. Sea $x \in \left[X_1 \intersection \Gamma^- F(\alpha') \intersection X(\beta') \right]$, recordemos que por construcción de la secuencia transfinita $\{ X(\alpha) \}$, $x \in X(\beta') \implies \Gamma x \subset X(\beta)$, para algún $\beta < \beta'$. Entonces $\Gamma x \intersection (F(\alpha') \intersection X(\beta)) \neq \phi$.

También por hipótesis de inducción, como $\beta < \alpha'$, entonces $F(\alpha') \intersection X(\beta) = F(\beta) \intersection X(\beta)$ de donde se deduce que $\Gamma x \intersection F(\beta) \neq \phi$, razón por la cual $x \in \Gamma^- F(\beta)$.

De esto se conluye que
\begin{align*}
x &\in X(\beta') \intersection X_1 \intersection \Gamma^- F(\beta) \\
	&\subset X(\beta') \intersection F(\beta+1) \\
	&\subset X(\beta') \intersection F(\beta'),
\end{align*}
donde la relación entre la primera y segunda ecuación está dada por la hipótesis de inducción y la forma en que se han construido los sucesores en $\{ F(\alpha) \}$.\\

Si tomamos $x \in [X_j \intersection \Gamma^+ F(\alpha') \intersection X(\beta')]$, $j \geq 2$, podemos ocupar un argumento similar ya que $\Gamma x \subset X(\beta)$, para algún $\beta < \beta' \leq \alpha$, y entonces $\Gamma x \subset F(\alpha') \intersection X(\beta) = F(\beta) \intersection X(\beta)$.

Entonces 
\begin{align*}
x &\in X(\beta') \intersection X_j \intersection \Gamma^+ F(\beta) \\
	&\subset X(\beta') \intersection F(\beta + 1) \\
	&\subset X(\beta') \intersection F(\beta').
\end{align*}
Lo cual concluye que $F(\alpha) \intersection X(\beta') = F(\beta') \intersection X(\beta')$.\\

Para ver que $F(\alpha) \intersection X(\alpha)$ es cerrado en $X(\alpha)$ tómese $x \in X(\alpha) \minus F(\alpha)$. Se considerarán tres casos para $x$. Primero considérese que $x \in X(\alpha')$, como $F(\alpha') \subset F(\alpha)$ entonces $x \notin F(\alpha')$ y como por hipótesis de inducción $F(\alpha') \intersection X(\alpha')$ es cerrado en $X(\alpha')$ entonces existe una vecindad abierta $U$ de $x$ en $X(\alpha')$ tal que $U \intersection F(\alpha') = \phi$. Como $X(\alpha')$ es abierto en $X$ y $U \subset X(\alpha') \subset X(\alpha)$, entonces $U$ es una vecindad abierta de $x$ en $X(\alpha)$, y ya que $X(\alpha') \intersection F(\alpha) = X(\alpha') \intersection F(\alpha')$, $U \intersection F(\alpha) = \phi$.

Para el segundo caso supóngase que $x \in (X(\alpha) \minus X(\alpha')) \intersection X_1$, entonces $\Gamma x \subset X(\alpha') \minus F(\alpha')$ por cómo está construido $F(\alpha)$ y el hecho de que $x \notin F(\alpha)$. $X(\alpha') \minus F(\alpha')$ es un conjunto abierto en $X(\alpha')$ y entonces también en $X$ y como $\Gamma$ es continua $X_1 \intersection \Gamma^+ X(\alpha') \minus F(\alpha')$ es una vecindad abierta de $x$ que no intersecta $F(\alpha)$.

En el tercer caso $x \in (X(\alpha) \minus X(\alpha')) \intersection X_j$, aquí se tiene que $\Gamma x \intersection X(\alpha') \minus F(\alpha') \neq \phi$ y $X_j \intersection \Gamma^- X(\alpha') \minus F(\alpha')$ es una vecindad abierta de $x$ que no intersecta $F(\alpha)$.\\

De este modo se ha probado que si $x \in X(\alpha) \minus F(\alpha)$ existe una vecindad abierta de $x$ en $X(\alpha)$ que no intersecta $F(\alpha)$, por tanto $X(\alpha) \minus F(\alpha)$ es abierto en $X(\alpha)$ y su complemento en $X(\alpha)$, $X(\alpha) \intersection F(\alpha)$ es cerrado.\\

Por inducción transfinita podemos construir conjuntos $F(\alpha)$ para cada ordinal $\alpha$ tal que cumplen las cuatro condiciones dadas. Si regresamos a los $X(\alpha)$, como el juego es localmente finito, debe existir un ordinal $\alpha_0$ tal que $X = X(\alpha_0)$ (Teorema \ref{thm:local_finito}). Así $F(\alpha_0)$ es un conjunto cerrado en $X$ y si $\alpha > \alpha_0$,
\begin{align*}
F(\alpha) &= F(\alpha) \intersection X\\
	&= F(\alpha) \intersection X(\alpha_0) \\
	&= F(\alpha_0) \intersection X(\alpha_0) \\
	&= F(\alpha_0) \intersection X \\
	&= F(\alpha_0).
\end{align*}
Sea $F' = X \minus F(\alpha_0)$. Si $x \in F' \intersection X_1$, entonces $\Gamma x \subset F'$ y si $x \in F' \intersection X_j$ con $j \neq 1$, $\Gamma x \intersection F' \neq \phi$. Entonces si la partida comienza en una posición de $F'$, sin importar que haga $(1)$, el resto puede asegurar que nunca se alcanza una posición en $F(\alpha_0)$. Pero $F(\alpha_0) \supset F(0) = \{ x: f_1(x) \geq \gamma \}$ y entonces $F_\gamma \subset F(\alpha_0)$ y como $F(\alpha_0) \subset F_\gamma$ por construcción entonces
$$
F(\alpha_0) = F_\gamma
$$
Lo cual concluye la prueba de que $F_\gamma$ es cerrado.
\end{proof}

Los siguientes dos teoremas heredan su demostración de los teoremas previos, así que no seré muy extenso en ellos.

\begin{theorem}
\label{thm:3}
Si un juego es topológico superior para $(1) \in N^-$, el conjunto de posiciones desde las cuales $(1)$ puede garantizar $\gamma$ es cerrado.
\end{theorem}

\begin{proof}
Sea $H_\gamma$ el conjunto de posiciones desde las cuales $(1)$ \emph{no puede} garantizar $\gamma$. De forma similar a la demostración del Teorema \ref{thm:1} podemos construir un conjunto abierto $H$ tal que $\{x \in X: f_1(x) < \gamma \} \subset H \subset H_y$, y si $x \in H' \intersection X_1$, donde $H' = X \minus H$, $\Gamma x \intersection H' \neq \phi$. mientras que si $x \in H' \intersection X_j (j \neq 1)$, $\Gamma x \subset H'$. Así si una partida comienza en una posición de $H'$, $(1)$ puede garantizar que nunca se alcanza una posición en $H$. Entonces si $F_\gamma$ es el conjunto de posiciones iniciales desde las cuales $(1)$ puede garantizar $\gamma$, $F_\gamma \supset H'$. Pero $H_\gamma \supset H$ y $F_\gamma \intersection H_\gamma = \phi$ y entonces $F_\gamma = H'$, es decir, $F_\gamma$ es el complemento de un conjunto abierto, por lo tanto es cerrado.
\end{proof}

\begin{theorem}
Supóngase que un juego es localmente finito y topológico inferior para $(1) \in N^-$ y que $X_0$ es un conjunto abierto. Entonces el conjunto de posiciones desde las cuales $(1)$ puede garantizar una ganancia estricta $\gamma$ es un conjunto abierto.
\end{theorem}

\begin{proof}
Sea $K_\gamma$ el conjunto de posiciones iniciales desde las cuales $(1)$ \emph{no puede} garantizar una ganancia estricta $\gamma$. Usando una ligera modificación del argumento usado para demostrar el Teorema \ref{thm:2} podemos mostrar que $K_\gamma$ es cerrado. Pero si $G_\gamma$ es el conjunto de posiciones iniciales desde las cuales $(1)$ puede garantizar una ganancia estricta $\gamma$, $G_\gamma = X - K_\gamma$ y entonces $G_\gamma$ es abierto.
\end{proof}

\end{section}

\begin{section}{Estrategias}
Ahora se definen las estrategias y un teorema que nos da condiciones suficientes para la existencia de estrategias óptimas.

\begin{definition}
Una \emph{estrategia}\index{estrategia} para el jugador $(i)$ es una función $\sigma:X_i \minus X_0 \to X$ tal que
$$
\sigma x \in \Gamma x \qquad
$$
\end{definition}

Es fácil observar que una estrategia para cada jugador y una posición inicial del juego determinan una \emph{partida} del mismo. Por esta razón a veces se podrá denotar una partida por $(x_0; \sigma)$ donde $x_0 \in X$ y $\sigma = (\sigma_1, \sigma_2, \dots, \sigma_N)$.

\begin{definition}
Se dice que una estrategia $\sigma$ para el jugador $(1)$ \emph{garantiza $\gamma$} desde la posición inicial $x$ si siempre que una partida comienza en $x$ y $(1)$ usa la estrategia $\sigma$, éste obtiene una ganancia mayor o igual a $\gamma$ sin importar qué hagan el resto de los jugadores.
\end{definition}

Si $x \in X$, sea
$$
\phi(x) = \sup \{\gamma \in \reals: x \in F_\gamma \},
$$
donde $F_\gamma$ es el conjunto de posiciones desde las cuales $(1)$ puede garantizar $\gamma$. Una estrategia para el jugador $(1)$ se dice \emph{óptima} si garantiza $\phi(x)$, para toda $x \in X$.

Se cumple que $\phi(x) < \infty$ ya que por la definición de juego (\ref{def:game}) $f_i < \infty$ para $i \in N$.

Denotemos por $\Sigma$ el conjunto de estrategias para el jugador $(1)$. Como cada estrategia para $(1)$ es una función de $X_1 \minus X_0$ a $X$, $\Sigma \subset X^{X_1 \minus X_0}$. Si dotamos a $\Sigma$ de la topología producto \footnote{Se puede ver el espacio producto $\displaystyle\Sigma = \prod_{x \in X_1 \minus X_0} \Gamma x$ como un subconjunto del espacio de funciones que van de $X_1 \minus X_0$ a $X$.} podemos probar el siguiente

\begin{theorem}
\label{thm:opt_strategies}
Supóngase que tenemos ya sea
\begin{enumerate}[\quad a.]
	\item un juego localmente finito topológico superior para $(1) \in N^+$ tal que $X_0$ es un conjunto abierto en $X$, ó
	\item un juego topológico superior para $(1) \in N^-$.
\end{enumerate}
Si $\Gamma x$ es compacto para toda $x \in X_1 \minus X_0$ entonces el conjunto de estrategias óptimas para $(1)$ es no vacío y cerrado en $\Sigma$.
\end{theorem}

\begin{proof}
Primero observamos que $\Sigma$ es compacto, ya que
$$
\Sigma = \prod_{x \in X_1 \minus X_0} \Gamma x
$$
y ya que $\Gamma x$ es compacto en $X$ para toda $x \in X_1 \minus X_0$, $\Sigma$ es compacto por el Teorema de Tychonoff \footnote{Establece que el producto de compactos (en este caso las imágenes $\Gamma x$ para cada $x \in X\\X_0$) es compacto en la topología producto.}.\\

Sea $S_\gamma$ el conjunto de estrategias con las cuales $(1)$ puede garantizar $\gamma$ para cualquier posición inicial en $F_\gamma$. Claramente $S_\gamma \neq \phi$ si $F_\gamma \neq \phi$.

Para probar que $S_\gamma$ es cerrado en $\Sigma$ sea $\sigma \in \Sigma$, $\sigma \notin S_\gamma$. Entonces para alguna $x \in X_1 \intersection F_\gamma$, $\sigma x \notin F_\gamma$. Pero con cualquiera de las condiciones (a) o (b), $F_\gamma$ es cerrado (Teoremas \ref{thm:2} y \ref{thm:3}) así que existe una vecindad abierta $N$ de $\sigma x$ en $X$ tal que $N \intersection F_\gamma = \phi$. Si $M(\sigma) = \{\tau \in \Sigma: \tau x \in N \}$, $M(\sigma)$ es una vecindad abierta de $\sigma$ y $M(\sigma) \intersection S_\gamma = \phi$. Entonces $S_\gamma$ es cerrado en $\Sigma$.

Ahora sea $x_0 \in X$, considérese la familia $\{S_\gamma: \gamma < \phi(x_0)\}$. Sean
$$
\gamma_1 < \gamma_2 < \dots < \gamma_n < \phi(x_0).
$$
Entonces $x_0 \in F_{\gamma_i}$ para cada $i$ y
$$
F_{\gamma_1} \supset F_{\gamma_2} \supset \dots F_{\gamma_n}.
$$
Supóngase que $F_{\gamma_k} \intersection X_1 \minus X_0 \neq \phi$ y que $k \leq n$ es el entero más grande para el cual esto es cierto. Entonces para $k < j \leq n$, $\Gamma F_{\gamma_j} \intersection X_1 \minus X_0 = \phi$ y $S_{\gamma j} = \Sigma$. Sea $\sigma_i \in S_{\gamma i}$ para $1 \leq i \leq k$, y considérese la estrategia $\sigma \in \Sigma$, donde para $x \in X_1 \minus X_0$,
$$
\sigma x = \begin{Bmatrix}
\sigma_k x & x \in F_{\gamma_k} \\
\sigma_i x & x \in F_{\gamma_i} - F_{\gamma_{i+1}} \\
\sigma_1 x & x \notin F_{\gamma_2}
\end{Bmatrix} \qquad (i = 2, \dots, k-1).
$$
Entonces $\sigma \in S_{\gamma_1} \intersection \dots \intersection S_{\gamma_n}$. Luego para cada $x_0 \in X$, $\{S_\gamma: \gamma < \phi(x)\}$ es una familia de conjuntos cerrados no vacíos con la propiedad de la intersección finita. Por lo que $S(x) = \bigintersection_{\gamma < \phi(x)} S_\gamma$ es un conjunto cerrado no vacío.

Ahora considere la familia $\{S(x): x \in X\}$. Suponga que $x_1, \dots, x_n \in X$. Si $x_m$ es tal que
$$
\phi(x_m) = \max_{1 \leq i \leq n} \phi(x_i),
$$
se tiene $S(x_m) \subset S(x_i)$. Entonces $\{S(x): x \in X\}$ es una familia de conjuntos cerrados no vacíos con la propiedad de la intersección finita. Sea $S = \bigintersection_{x \in X} S(x)$. Así $S$ es no vacío y cerrado en $\Sigma$.

Veremos que $S$ es precisamente el conjunto de estrategias óptimas para $(1)$. Si tomamos $\sigma \in S(x)$, $\sigma \in S_\gamma$ para toda $\gamma < \phi(x)$. Por lo cual si $P$ es una partida que comienza en $x$ en la que $(1)$ jugó $\sigma$ entonces $g_1(P) \geq \gamma, \forall \gamma < \phi(x)$. O lo que es lo mismo, $g_1(P) = \phi(x)$ y por ende $(1)$ garantiza $\phi(x)$ usando $\sigma$. Luego como $\sigma \in \bigintersection_{x \in X} S(x)$, $\sigma$ es una estrategia óptima pues garantiza $\phi(x)$ para cada $x \in X$.

De la misma manera, si $\sigma$ es una estrategia óptima para $(1)$, $\sigma$ le garantiza a $(1)$ una ganancia $\phi(x)$ si la partida comienza en $x$ y así $\sigma \in \bigintersection_{\gamma < \phi(x)} S_\gamma$. Esto es cierto para toda $x \in X$ así que $\sigma \in S = \bigintersection_{x \in X} \bigintersection_{\gamma < \phi(x)} S_\gamma$.
\end{proof}

\end{section}

\end{chapter}
