\begin{chapter}{Preliminares}
\begin{section}{Inducción transfinita}

Los números ordinales extienden la idea de los números naturales y los conceptos de \textit{sucesor} y \textit{buen orden} hacia lo transfinito, proporcionando una herramienta útil para la demostración de propiedades en conjuntos que pudieran ser no numerables. En el presente trabajo la inducción transfinita será una herramienta recurrente. Para construir esta sección se han utilizado demostraciones de \cite{thomas_jeck}.\\

Como parte de la notación se usará un subíndice en los símbolos $<$ y $\in$ para denotar la restricción de la operación a un conjunto determinado, así $<_\alpha$ es la restricción de $<$ al conjunto $\alpha$.\\

\begin{definition}
Un conjunto $A$ se dice \textit{transitivo} si cada elemento de $A$ es un subconjunto de $A$.
\end{definition}

\begin{definition}
Un conjunto $\alpha$ es un ordinal\index{ordinal} si
\begin{itemize}
	\item $\alpha$ es transitivo,
	\item $\alpha$ está bien ordenado por $\in$, (si $x, y \in \alpha$ entonces $x < y$ si $x \in y$).
\end{itemize}
\end{definition}

Es fácil observar que los números naturales en su construcción dada por medio de la noción de sucesor ($S(a) = a \union \{a\}$, $a_0 = \phi$) forman un conjunto transitivo y están bien ordenados, así pues forman un conjunto de ordinales que de hecho se conoce como \textit{conjunto de ordinales finitos}, pues son los únicos con esta propiedad. El primer ordinal infinito es el conjunto de los números naturales $\N = \bigunion_{\alpha \in \N} \alpha$. Este ordinal no es el sucesor, en términos de la definición, de ningún ordinal, sin embargo \textit{sucede} a sus predecesores por medio de la relación $\in$ y respeta las condiciones de transitividad y buen orden. Esto nos da la idea de dos formas de construir números ordinales: como sucesores o como uniones de todos los ordinales anteriores. En lo sucesivo se usará la notación $\alpha+1$ para denotar el sucesor de $\alpha$, i.e. $\alpha+1 = S(\alpha) = \alpha \union \{\alpha\}$.

\begin{definition}
Un ordinal $\alpha$ se llama \textit{ordinal sucesor} si existe un ordinal $\beta$ tal que $\alpha = \beta + 1$. En otro caso le llamaremos \textit{ordinal límite}.
\end{definition}

\begin{definition}
Para todos los ordinales $\alpha$ y $\beta$ definimos $\alpha<\beta$ si $\alpha \in \beta$.
\end{definition}

A continuación un par de lemas necesarios para demostrar el teorema que enuncia el principio de inducción transfinita.

\begin{lemma}
\label{lm:notin}
Si $\alpha$ es un ordinal entonces $\alpha \notin \alpha$.
\end{lemma}

\begin{proof}
Si $\alpha \in \alpha$ entonces el conjunto totalmente ordenado $(\alpha, <_\alpha)$ tiene un elemento $x = \alpha$ tal que $x <_\alpha x$, que contradice la asimetría de $<_\alpha$.
\end{proof}

\begin{lemma}
\label{lm:subset}
Si $\alpha$ y $\beta$ son ordinales tal que $\alpha \subsetneq \beta$ entonces $\alpha \in \beta$.
\end{lemma}

\begin{proof}
Sea $\alpha \subsetneq \beta$, entonces $\beta \minus \alpha$ es un subconjunto no vacío de $\beta$ y tiene un elemento mínimo $\gamma$ en el orden $<_\beta$. Nótese que $\gamma \subseteq \alpha$ pues de lo contrario cada $\delta \in \gamma \minus \alpha$ sería un elemento de $\beta \minus \alpha$ más pequeño que $\gamma$, pero $\gamma = \min \beta \minus \alpha$.\\

Para demostrar que $\alpha \subseteq \gamma$ tomemos $\delta \in \alpha$. Observemos que $\delta, \gamma \in \beta$ y además $\beta$ está totalmente ordenado por $\in_\beta$ entonces por tricotomía $\delta \in \gamma$ ó $\delta = \gamma$ ó $\gamma \in \delta$, pero estos dos últimos no pueden suceder pues la transitividad en $\alpha$ llevaría a que $\gamma \in \alpha$, que contradice la elección de $\gamma \in \beta \minus \alpha$.\\

Como $\alpha \subseteq \gamma$ y $\gamma \subseteq \alpha$ concluimos a que $\alpha = \gamma \in \beta$.
\end{proof}

Finalmente dotaremos de un orden total para cualquier conjunto de ordinales en el siguiente teorema:

\begin{theorem}
\label{thm:total}
Sean $\alpha$ y $\beta$ ordinales, se ha de cumplir que $\alpha < \beta$, $\alpha = \beta$ ó $\beta < \alpha$.
\end{theorem}

\begin{proof}
Si $\alpha$ y $\beta$ son ordinales entonces $\alpha \intersection \beta$ está bien ordenado por $<_\alpha$. También esta intersección es transitiva: tómese $x \in \alpha \intersection \beta$, como $x \in \alpha$ y $\alpha$ es transitivo $x \subset \alpha$, de la misma manera $x \subset \beta$ y entonces $x \subset \alpha \intersection \beta$. De esta manera $\alpha \intersection \beta$ es un ordinal.\\

Si $\alpha \intersection \beta = \alpha$ entonces $\alpha \subset \beta$ y por el Lema \ref{lm:subset} $\alpha \in \beta$ ó $\alpha = \beta$. De la misma manera $\alpha \intersection \beta = \beta$ implica $\beta \in \alpha$ ó $\beta = \alpha$.\\

Finalmente observamos que $\alpha \intersection \beta$ no puede ser un subconjunto propio de ambos $\alpha$ y $\beta$ a la vez pues en ese caso $\alpha \intersection \beta \in \alpha$ y $\alpha \intersection \beta \in \beta$ lo que lleva a que $\alpha \intersection \beta \in \alpha \intersection \beta$, que contradice el Lema \ref{lm:notin}.
\end{proof}

El siguiente teorema nos será útil para enunciar el principio de inducción transfinita y describe una propiedad escencial de los números ordinales

\begin{theorem}
Cada conjunto no vacío de ordinales tiene un elemento mínimo, esto quiere decir que cada conjunto de ordinales está bien ordenado por $<$.
\end{theorem}

\begin{proof}
Sea $A$ un conjunto no vacío de ordinales. Tómese $\alpha \in A$. Si $\alpha \intersection A = \phi$ tómese $\beta \in A$, por el Teorema \ref{thm:total} podemos comparar $\alpha$ y $\beta$ y dado que $\beta \notin \alpha$ entonces bien $\alpha \in \beta$ ó $\alpha = \beta$, lo cual nos lleva a que $\alpha$ es el elemento mínimo de $A$. Si por otro lado $\alpha \intersection A \neq \phi$ entonces $\alpha \intersection A$ tiene un elemento mínimo en el orden $<_\alpha$, digamos $\gamma$, que resulta ser el mínimo de $A$ ya que si tomamos cualquier otro $\beta \in A$, y pensáramos que $\beta < \gamma$ entonces $\beta \in \gamma \in \alpha$ y por transitividad $\beta \in \alpha$, luego $\beta \in \alpha \intersection A$ y como $\gamma$ es el mínimo de $\alpha \intersection A$ entonces se tendría $\gamma \in \beta$, que contradice el la tricotomía de $<$.
\end{proof}

A continuación se enuncia el principio de inducción transfinita\index{inducción transfinita}, que extiene el principio de inducción que ya conocemos sobre los números naturales.

\begin{theorem}
Sea $P(x)$ una propiedad de conjuntos, supóngase que para cada ordinal $\alpha$
$$
(P(\beta) \text{ se cumple } \forall \beta<\alpha) \implies P(\alpha).
$$
Entonces $P(\alpha)$ se cumple para todos los ordinales $\alpha$.
\end{theorem}

\begin{proof}
Supongamos que algún ordinal $\gamma$ no tiene la propiedad $P$ y sea $S$ el conjunto de todos los ordinales $\beta \leq \gamma$ que no tienen la propiedad $P$. Como cada conjunto de ordinales es bien ordenado $S$ tiene un elemento mínimo $\alpha$, pero como cada $\beta<\alpha$ tiene la propiedad $P$ se sigue por el enunciado del teorema que $P(\alpha)$ es cierta, lo cual contradice nuestra hipótesis. Entonces $P$ es cierta para cada ordinal $\alpha$.
\end{proof}

Aunque el teorema anterior ya enuncia el principio de inducción transfinita, para los fines que a esta tesis respectan hará falta otra forma del enunciado que nos permita considerar de manera distinta los ordinales límite y los ordinales sucesor. Esta forma se enuncia a continuación.

\begin{theorem}
Sea $P(x)$ una propiedad de conjuntos. Asuma que
\begin{enumerate}
	\item $P(0)$ se cumple,
	\item $P(\alpha)$ implica $P(\alpha + 1)$ para cada ordinal $\alpha$,
	\item para cada ordinal límite $\alpha \distinct 0$, si $P(\beta)$ se cumple $\forall \beta<\alpha$ entonces $P(\alpha)$ se cumple.
\end{enumerate}
Entonces $P(\alpha)$ es cierta para todo ordinal $\alpha$.
\end{theorem}

\begin{proof}
Basta con ver que dado el enunciado de este teorema se cumplen las hipótesis del anterior. Sea $\alpha$ un ordinal tal que $P(\beta)$ es cierto $\forall \beta < \alpha$. Si $\alpha=0$ entonces $P(\alpha)$ se cumple por (1). Si $\alpha$ es sucesor de algún $\beta$ sabemos que $P(\beta)$ se cumple, entonces $P(\alpha)$ es cierta por (2). Si $\alpha \neq 0$ es un ordinal límite $P(\alpha)$ se cumple por (3).
\end{proof}

El siguiente lema y lema \ref{lm:star} (\emph{Estabilidad de sucesiones transfinitas acotadas crecientes}) van a ser de gran utilidad para concluir importantes propiedades de conjuntos en secuencias transfinitas. La idea es que una secuencia transfinita de conjuntos no puede crecer hasta lo absurdo, sino que eventualmente tiene que estabilizarse y ser constante.

\begin{definition}
Para cualquier conjunto $A$, sea $H(A)$ el ordinal más pequeño que no es equipotente a ningún subconjunto de $A$. $H(A)$ también es conocido como Número de Hartogs de $A$.
\end{definition}

Por sí misma esta definición carece de peso. Sin embargo es posible demostrar que el número de Hartogs existe para cualquier conjunto. Dicha demostración se ha dejado al lector curioso, aunque también se puede encontrar en \cite{thomas_jeck}.

\begin{lemma}
\label{lm:inj}
Sea $\{G(\alpha)\}$ una sucesión transfinita de conjuntos con $G(0) \neq \phi$ y suponga que $\alpha < \beta \implies G(\alpha) \subsetneq G(\beta)$. Entonces para cada ordinal $\alpha$ existe una inyección de $\alpha$ a $G(\alpha)$.
\end{lemma}

\begin{proof}
Para $\alpha = 0$ dicha inyección se puede asumir por vacuidad, pues $0 = \phi$. En cualquier otro caso la función que puede inyectar $\beta$ en $G(\beta)$ se puede definir como sigue:
$$
f(\alpha) = x;\qquad x \in G(\alpha +1) \setminus G(\alpha); \qquad \text{si }\alpha < \beta.
$$
Aprovechando que la sucesión es estrictamente creciente el axioma de elección nos permite tomar un $x$ distinto en cada diferencia sucesiva de conjuntos, definiendo así la deseada función inyectiva.
\end{proof}

\begin{lemma}
\label{lm:star}
(Estabilidad de sucesiones transfinitas acotadas crecientes) Sea $\{G(\alpha)\}$ una sucesión transfinita de conjuntos con $G(0) \neq \phi$, y la propiedad
$$
\alpha < \beta \implies G(\alpha) \subset G(\beta),
$$
donde además para cada $\alpha$ se tiene que $G(\alpha) \subset X$. Entonces $\{G(\alpha)\}$ debe ser eventualmente constante.
\end{lemma}

La intuición dictaba que probar esto sería sencillo, es decir, ¿una secuencia transfinita creciente? Seguro que es capaz de agotar cualguier conjunto en algún momento. Sin embargo el trabajo con conjuntos de cardinalidades no numerables es bastante más delicado como se verá a continuación.\\

\begin{proof}
Se procederá por contradicción. Supongamos que para cada ordinal $\alpha$ existe un ordinal $\beta_\alpha > \alpha$ tal que
\begin{equation}
\label{eq:prop}
G(\alpha) \subsetneq G(\beta_\alpha)
\end{equation}
Entonces se puede construir una secuencia transfinita estrictamente creciente como sigue:

$$
\begin{array}{rcl}
	U_0 & = & G(0) \\
	U_1 & = & G(\beta_0) \\
	U_2 & = & G(\beta_{\beta_0}) \\
	& \vdots &
\end{array}
$$
Donde $n < m \implies U_n \subsetneq U_m$.\\

En general supóngase que para un ordinal $\gamma$ se han definido $U_\alpha$, $\forall \alpha < \gamma$ con la propiedad $\alpha < \beta < \gamma \implies U_\alpha \subsetneq U_\beta$.
\begin{itemize}
	\item Si $\gamma$ es un sucesor ($\gamma = \alpha + 1$) entonces se puede construir $U_\gamma$ a partir de $U_\alpha$ usando la propiedad \ref{eq:prop} descrita un par de líneas arriba como sigue: sea $\beta_\alpha$ tal que $G(\alpha) \subsetneq G(\beta_\alpha)$, entonces sea $U_\gamma = G(\beta_\alpha)$.

	\item Si $\gamma$ es un ordinal límite entonces sea $\alpha$ el supremo de los ordinales que han sido usados como argumento para $G$ y luego defínase $U_\gamma$ como $G(\beta_\alpha)$ usando la propiedad \ref{eq:prop}.
\end{itemize}

De esta manera se ha definido una secuencia transfinita $U_\alpha$ con la propiedad
$$
\alpha < \beta \implies U_\alpha \subsetneq U_\beta.
$$
Y donde cada $U_\alpha$ es un subconjunto de $X$. Si aplicamos el Lema \ref{lm:inj} entonces cada ordinal $\alpha$ se puede inyectar en su respectivo $U_\alpha$. En partícular el número de Hartogs $H(X)$ se puede inyectar en $U_{H(X)} \subset X$, ¡lo cual es una contradicción!.\\

Entonces existe $\alpha_0$ tal que $G(\alpha) = G(\alpha_0)$, $\forall \alpha > \alpha_0$.
\end{proof}

\end{section}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{section}{Juegos topológicos}
Para el presente trabajo consideraremos un juego topológico como lo describe Claude Berge en su artículo \cite{claude_berge}, es decir, uno en el que las posiciones del juego son elementos de un espacio topológico y el conjunto de jugadas posibles desde un punto dado está definido por una función del espacio al conjunto potencia. Esto nos dará una estructura apropiada para estudiar las propiedades del juego en términos de la topología que lo describe.\\

Las siguientes definiciones nos ayudarán a construir una definición apropiada de un juego topológico. Para denotar al $i$-ésimo jugador se usará la notación $(i)$ que nos permite distinguir el objeto \emph{jugador} de su índice en ciertas circunstancias.

\begin{definition}
Una función $f:X \to \reals$, con $X$ un espacio topológico, se dice \emph{superiormente semicontinua}\index{superiormente semicontinua} en $x_0$ si para cada $\epsilon > 0$ existe una vecindad $U$ de $x_0$ tal que $f(x) \leq f(x_0) + \epsilon$ para toda $x \in U$.\\

De la misma forma se puede definir la semicontinuidad inferior en $x_0$ si para cada $\epsilon > 0$ existe una vecindad $U$ de $x_0$ tal que $f(x) \geq f(x_0) - \epsilon$ para toda $x \in U$.\\

Finalmente una función será superior o inferiormente semicontinua si es superior o inferiormente semicontinua en cada punto donde está definida.
\end{definition}

\begin{definition}
Un \emph{mapeo multivaluado}\index{mapeo multivaluado} de un conjunto $X$ a un conjunto $Y$ es una función $\Gamma:X \to 2^Y$
\end{definition}

\begin{definition}
\label{def:inv}
Si $\Gamma:X \to 2^Y$ es un mapeo multivaluado y $A \in 2^Y$ se definen la \emph{inversa superior}\index{inversa superior} y la \emph{inversa inferior}\index{inversa inferior} de $\Gamma$ como
$$
\Gamma^+ A = \{x \in X: \Gamma x \neq \phi, \Gamma x \subset A\},
$$
y
$$
\Gamma^- A = \{x \in X: \Gamma x \intersection A \neq \phi\},
$$
respectivamente.
\end{definition}

Nótese que ambas, la inversa superior y la inversa inferior, son monótonas ya que si $A \subset B$ entonces $\Gamma^- A \subset \Gamma^- B$ y $\Gamma^+ A \subset \Gamma^+ B$ son ciertas \footnote{Sea $x \in \Gamma^- A$, i.e. $\Gamma x \intersection A \neq \phi$. Como $A \subset B$, $\Gamma x \intersection B \neq \phi$, así $x \in \Gamma^- B$.} \footnote{Sea $x \in \Gamma^+ A$, i.e. $\Gamma x \neq \phi$, $\Gamma x \subset A \subset B$. Así $x \in \Gamma^+ B$.}.

\begin{definition}
Sean $X$, $Y$ espacios topológicos y $\Gamma:X \to 2^Y$ un mapeo multivaluado. Decimos que $\Gamma$ es continua si por cada abierto $U \subset Y$, $\Gamma^+U$ y $\Gamma^-U$ son abiertos en $X$.
\end{definition}

Dicho esto, podemos definir un juego topológico de la siguiente manera:

\begin{definition}
\label{def:game}
Un \emph{juego topológico}\index{juego topológico} en un conjunto de casillas $X$ para los jugadores $(1), (2), \dots, (n) \in N$ ($N$ denota aquí el conjunto de jugadores) está definido por
\begin{enumerate}
	\item una partición $\{N^+, N^-\}$, del conjunto $N$ de jugadores,
	\item una partición $\{X_1, X_2, \dots, X_n\}$ de $X$ donde cada $X_i$ está dotado de una topología,
	\item un mapeo multivaluado $\Gamma$ de $X$ a sí mismo tal que si $x \in X_i$ entonces
	$$
	\Gamma x \intersection X_i = \phi \text{ para } i=1, 2, \dots, n,
	$$
	\item $n$ funciones reales acotadas definidas en $X$: $f_1, f_2, \dots f_n$.
\end{enumerate}
\end{definition}

Un juego construido de esta manera será denotado por $\game = (X, \Gamma, N, \{f_i\}_{i \in N})$. En esta forma de plantear un juego los elementos $x \in X_i$ de cada partición de $X$ significan que al jugador $(i)$ le corresponde la iniciativa desde la casilla $x$. El juego puede iniciar en cualquier punto $x \in X$ y $X$ tiene la topología suma. Así mismo se usará $X_0 = \{x \in X, \Gamma x = \phi \}$ para denotar las casillas que terminan el juego.\\

Un juego que inicie en $x_0$ procede de la siguiente manera: si $x_0 \in X_i$, el jugador $(i)$ escoge una posición $x_1$ en el conjunto $\Gamma x_0$; si $x_1 \in X_j$, el jugador $(j)$ escoge una posición $x_2 \in \Gamma x_1$ y así se siguen. Si en el transcurso del juego se alcanza una posición $x$ tal que $\Gamma x = \phi$ entonces el juego termina en $x$.\\

Las siguientes definiciones nos ayudarán a construir las bases para demostrar propiedades sobre los juegos topológicos relativas a la existencia de estrategias ganadoras

\begin{definition}
Una \emph{partida}\index{partida} es una secuencia $\{x_0, x_1, \dots\}$ donde $x_{j+1} \in \Gamma x_j$
\end{definition}

Si la secuencia de jugadas es finita y tiene $k+1$ elementos decimos que la partida es de longitud $k$ siempre y cuando el último elemento $x_k$ cumpla que $\Gamma x_k = \phi$. Si la longitud de cada partida de un juego dado es finita diremos que el juego es \emph{localmente finito}\index{localmente finito}.

\begin{theorem}
\label{thm:local_finito}
Un juego es localmente finito si y solo si tiene un número ordinal. Esto es, existe un ordinal $\alpha$ tal que se cumple $X(\alpha) = X$.

Donde $X(0) = X_0$ y si $\alpha$ es sucesor de $\alpha'$ entonces
$$
X(\alpha) = X(\alpha') \union \Gamma^+ X(\alpha')
$$
y si $\alpha$ es límite entonces
$$
X(\alpha) = \bigunion_{\beta < \alpha} X(\beta).
$$ \footnote{Este teorema viene de \cite{berge_french}}
\end{theorem}

\begin{proof}
Primero se demostrará que si un juego es localmente finito entonces tiene un número ordinal. Se procede por contradicción.\\

Sea $\game$ un juego localmente finito que no posee un número ordinal. Sea $A$ el conjunto de posiciones de $X$ que no pertenecen a ningún conjunto de la forma $X(\alpha)$, como $\game$ no tiene un número ordinal $A$ es no vacío. Si $x_1 \in A$ entonces $\Gamma x_1 \neq \phi$ (pues $x_1 \notin X_0=X(0)$) así que existe $x_2 \in \Gamma x_1$ tal que $\Gamma x_2 \neq \phi$ (ya que en otro caso $x_1 \in X(0) \union \Gamma^+ X(0) = X(1)$) y existe $x_3 \in \Gamma x_2$ con $\Gamma x_3 \neq \phi$ y este proceso se puede repetir infinitamente para construir una partida
$$
\{x_1, x_2, x_3, \dots \}
$$
infinita, lo cual contradice la hipótesis.\\

Ahora supóngase que $\game$ tiene un número ordinal y sea $S$ una partida de longitud infinita. Considérese $x \in S$. Las partidas que comienzan en $X_0$ son de longitud finita así que $x \notin X_0$.

Si $\alpha$ es un ordinal sucesor, digamos, de $\alpha'$ y las partidas que comienzan en cualquier punto de $X(\alpha')$ son finitas entonces las partidas que comienzan en $X(\alpha) = X(\alpha') \union \Gamma^+ X(\alpha')$ son finitas y entonces $x \notin X(\alpha)$.

Si $\alpha$ es un ordinal límite y para todo $\beta < \alpha$ las partidas que comienzan en $X(\beta)$ son finitas entonces como $X(\alpha) = \bigunion_{\beta < \alpha} X(\beta)$ si $x \in X(\alpha)$, $x \in X(\beta)$ para algún $\beta < \alpha$ y toda partida que empiece en $x$ es finita, así que $x \notin X(\alpha)$.

Finalmente por el principio de inducción transfinita $x \notin X(\alpha)$ para ningún $\alpha$ y como $\game$ tiene un número ordinal ($X = X(\alpha)$)se concluye $x \notin X$. Esto es para cualquier $x \in S$. Así si $S$ es una partida de longitud infinita no puede pertenecer a $X$.

Así por el principio de inducción transfinita $x \notin X$ ya que $X = X(\alpha)$ para algún ordinal $\alpha$. Dicho de otra manera, ninguna pardida de longitud infinita está contenida en $X$.
\end{proof}

\begin{definition}
Si $S$ es el conjunto de jugadas de una partida, definimos la \emph{ganancia}\index{ganancia} del jugador $(i)$ en la partida $S$ como
$$
g_i(S) = \left\{
	\begin{array}{lr}
		\sup \{f_i(x): x \in S\}, & (i) \in N^+ \\
		\inf \{f_i(x): x \in S\}, & (i) \in N^-
	\end{array}
\right.
$$
según si $(i) \in N^+$ ó $(i) \in N^-$.
\end{definition}

Al hacer esta distinción en la forma de calcular la ganancia de cada jugador estamos considerando jugadores \emph{activos} si $(i)\in N^+$ y \emph{pasivos} si $(i) \in N^-$. Los primeros buscarán conseguir las posiciones que maximicen sus ganancias mientras que los segundos buscarán minimizar sus pérdidas.\\

\begin{definition}
Se dice que el jugador $(i)$ \emph{garantiza $\gamma$}\index{garantizar $\gamma$}, con $\gamma$ un número real, desde la posición $x$ si para cada secuencia de jugadas $P = \{x, x_1, \dots, x_{k_\lambda} \}$ con $x_{i+1} \in \Gamma x_i$ y $x_{k_\lambda} \in X_i$ sucede que $g_i(P) \geq \gamma$ o bien existe un $y \in \Gamma x_{k_\lambda}$ tal que $\{x, x_1, \dots, x_{k_\lambda}, y \}$ pertenece a una partida $S$ con $g_i(S) \geq \gamma$. Si $g_i(S) > \gamma$ ($g_i(P) \geq \gamma$) se dice que $(i)$ \emph{garantiza estrictamente} $\gamma$.
\end{definition}

Un hecho interesante es que la definición anterior dice \emph{desde la posición $x$}, lo cual puede hacer pensar que se garantiza una ganancia si el juego comienza en $x$. Sin embargo lo que realmente sucede es que en los teoremas se estudia un juego \emph{a partir} de la posición $x$ (a la que a veces se le dirá \emph{inicial}) aunque $x$ se encuentre a media partida.\\

La última definición de esta sección nos dice cómo podemos utilizar la estructura de las definiciones anteriores para caracterizar un juego sobre el cual podremos demostrar teoremas muy interesantes acerca de la existencia o no de estrategias ganadoras.

\begin{definition}
Sea $\game = (X, \Gamma, N, \{f_i\}_{i \in N})$ un juego topológico. $\game$ se dice \emph{topológico inferior para el jugador $(i)$}\index{topológico inferior/superior} si además la función real $f_i$ es inferiormente semicontinua. El juego se dice \emph{superiormente topológico para el jugador $(i)$} si $f_i$ es superiormente semicontinua.
\end{definition}

Dicha toda esta zarta de definiciones inteligibles y completamente arbitrarias podemos pasar con los teoremas gordos de la tesis \footnote{¿Acaso no es toda definición una arbitrariedad?}.

\end{section}

\end{chapter}
